import A from './src/elements/a'
import Circle from './src/elements/circle'
import ForeignObject from './src/elements/foreignobject'
import G from './src/elements/g'
import Line from './src/elements/line'
import Path from './src/elements/path'
import Polygon from './src/elements/polygon'
import Rect from './src/elements/rect'
import Style from './src/elements/style'
import Text from './src/elements/text'
import type { AnchorAttributes } from './src/elements/a'
import type { CAttributes } from './src/elements/circle'
import type { ForeignObjectAttributes } from './src/elements/foreignobject'
import type { GAttributes } from './src/elements/g'
import type { LineAttributes } from './src/elements/line'
import type { PathAttributes } from './src/elements/path'
import type { PolygonAttributes } from './src/elements/polygon'
import type { RectAttributes } from './src/elements/rect'
import type { StyleAttributes } from './src/elements/style'
import type { TextAttributes } from './src/elements/text'

class SvgBuilder {
  private root: string
  private elements: string[]
  constructor() {
    this.root = `<svg height="100" width="100" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">`
    this.elements = []
  }

  private formatRoot(name: string, value: string): string {
    const regexp = new RegExp(`${name}([^=,]*)=("[^"]*"|[^,"]*)`)
    return this.root.replace(regexp, `${name}="${value}"`)
  }

  private closeTag(name: string): string {
    return `</${name}>`
  }

  width(value: number): SvgBuilder {
    this.root = this.formatRoot('width', value.toString())
    return this
  }

  height(value: number): SvgBuilder {
    this.root = this.formatRoot('height', value.toString())
    return this
  }

  viewBox(value: number): SvgBuilder {
    this.root = this.root.replace('<svg ', `<svg viewBox="${value.toString()}" `)
    return this
  }

  addElement(element: { node: string, content?: SvgBuilder, name: string }): SvgBuilder {
    const { node, content, name } = element

    if (!content) {
      this.elements.push(`${node}${this.closeTag(name)}`)
    }
    else if (content instanceof SvgBuilder) {
      this.elements = []
      this.elements.unshift(node, content.render())
      this.elements.push(this.closeTag(name))
    }
    else if (typeof content === 'object') {
      const elements = this.elements.join('')
      this.elements = []
      this.elements.unshift(node, elements)
      this.elements.push(this.closeTag(name))
    }
    return this
  }

  newInstance(): SvgBuilder {
    return new SvgBuilder()
  }

  reset() {
    this.elements = []
  }

  render() {
    return this.root + this.elements.join('') + this.closeTag('svg')
  }

  // eslint-disable-next-line node/prefer-global/buffer
  buffer(): Buffer {
    // eslint-disable-next-line node/prefer-global/buffer
    return Buffer.from(this.render())
  }

  anchor(attrs: AnchorAttributes, content?: SvgBuilder) {
    this.addElement(new A(attrs, content))
    return this
  }

  circle(attrs: CAttributes, content?: SvgBuilder) {
    this.addElement(new Circle(attrs, content))
    return this
  }

  foreignObject(attrs: ForeignObjectAttributes, content?: SvgBuilder) {
    this.addElement(new ForeignObject(attrs, content))
    return this
  }

  g(attrs: GAttributes, content?: SvgBuilder) {
    this.addElement(new G(attrs, content))
    return this
  }

  line(attrs: LineAttributes, content?: SvgBuilder) {
    this.addElement(new Line(attrs, content))
    return this
  }

  rect(attrs: RectAttributes, content?: SvgBuilder) {
    this.addElement(new Rect(attrs, content))
    return this
  }

  path(attrs: PathAttributes, content?: SvgBuilder) {
    this.addElement(new Path(attrs, content))
    return this
  }

  polygon(attrs: PolygonAttributes, content?: SvgBuilder) {
    this.addElement(new Polygon(attrs, content))
    return this
  }

  style(attrs: StyleAttributes, content?: SvgBuilder) {
    this.addElement(new Style(attrs, content))
    return this
  }

  text(attrs: TextAttributes, content?: SvgBuilder) {
    this.addElement(new Text(attrs, content))
    return this
  }
}

export default new SvgBuilder()
