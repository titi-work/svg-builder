declare module '@svg-builder/types/attributes' {
  export interface PositionAttributes {
    x1?: number
    y1?: number
    x2?: number
    y2?: number
  }

  export interface GraphicalEventAttributes {
    onactivate?: string
    onclick?: string
    onfocusin?: string
    onfocusout?: string
    onload?: string
    onmousedown?: string
    onmousemove?: string
    onmouseout?: string
    onmouseover?: string
    onmouseup?: string
  }

  export interface CoreAttributes {
    class?: string
    id?: string
    lang?: string
    style?: string
    tabindex?: string
    xmlBase?: string
    xmlLang?: string
    xmlSpace?: string
  }

  export interface ConditionalProcessingAttributes {
    requiredExtensions?: string
    requiredFeatures?: string
    systemLanguage?: string
  }

  export interface AnimationEventAttributes {
    onbegin?: string
    onend?: string
    onload?: string
    onrepeat?: string
  }

  export interface AnimationAttributeTargetAttributes {
    attributeType?: string
    attributeName?: string
  }

  export interface AnimationTimingAttributes {
    begin?: string
    dur?: string
    end?: string
    min?: string
    max?: string
    restart?: string
    repeatCount?: string
    repeatDur?: string
    fill?: string
  }

  export interface AnimationValueAttributes {
    calcMode?: string
    values?: string
    keyTimes?: string
    keySplines?: string
    from?: string
    to?: string
    by?: string
  }

  export interface AnimationAdditionAttributes {
    additive?: string
    accumulate?: string
  }

  export interface AriaAttributes {
    'aria-activedescendant'?: string
    'aria-atomic'?: string
    'aria-autocomplete'?: string
    'aria-busy'?: string
    'aria-checked'?: string
    'aria-colcount'?: string
    'aria-colindex'?: string
    'aria-colspan'?: string
    'aria-controls'?: string
    'aria-current'?: string
    'aria-describedby'?: string
    'aria-details'?: string
    'aria-disabled'?: string
    'aria-dropeffect'?: string
    'aria-errormessage'?: string
    'aria-expanded'?: string
    'aria-flowto'?: string
    'aria-grabbed'?: string
    'aria-haspopup'?: string
    'aria-hidden'?: string
    'aria-invalid'?: string
    'aria-keyshortcuts'?: string
    'aria-label'?: string
    'aria-labelledby'?: string
    'aria-level'?: string
    'aria-live'?: string
    'aria-modal'?: string
    'aria-multiline'?: string
    'aria-multiselectable'?: string
    'aria-orientation'?: string
    'aria-owns'?: string
    'aria-placeholder'?: string
    'aria-posinset'?: string
    'aria-pressed'?: string
    'aria-readonly'?: string
    'aria-relevant'?: string
    'aria-required'?: string
    'aria-roledescription'?: string
    'aria-rowcount'?: string
    'aria-rowindex'?: string
    'aria-rowspan'?: string
    'aria-selected'?: string
    'aria-setsize'?: string
    'aria-sort'?: string
    'aria-valuemax'?: string
    'aria-valuemin'?: string
    'aria-valuenow'?: string
    'aria-valuetext'?: string
    'role'?: string
  }

  export interface DocumentEventAttributes {
    onabort?: string
    onerror?: string
    onresize?: string
    onscroll?: string
    onunload?: string
    onzoom?: string
  }

  export interface FilterPrimitiveAttributes {
    height?: number
    result?: string
    width?: number
    x?: number
    y?: number
  }

  export interface PresentationAttributes {
    'alignment-baseline'?: string
    'baseline-shift'?: string
    'clip'?: string
    'clip-path'?: string
    'clip-rule'?: string
    'color'?: string
    'color-interpolation'?: string
    'color-interpolation-filters'?: string
    'color-profile'?: string
    'color-rendering'?: string
    'cursor'?: string
    'direction'?: string
    'display'?: string
    'dominant-baseline'?: string
    'enable-background'?: string
    'fill'?: string
    'fill-opacity'?: string
    'fill-rule'?: string
    'filter'?: string
    'flood-color'?: string
    'flood-opacity'?: string
    'font-family'?: string
    'font-size'?: string
    'font-size-adjust'?: string
    'font-stretch'?: string
    'font-style'?: string
    'font-variant'?: string
    'font-weight'?: string
    'glyph-orientation-horizontal'?: string
    'glyph-orientation-vertical'?: string
    'image-rendering'?: string
    'kerning'?: string
    'letter-spacing'?: string
    'lighting-color'?: string
    'marker-end'?: string
    'marker-mid'?: string
    'marker-start'?: string
    'mask'?: string
    'opacity'?: string
    'overflow'?: string
    'pointer-events'?: string
    'shape-rendering'?: string
    'stop-color'?: string
    'stop-opacity'?: string
    'stroke'?: string
    'stroke-dasharray'?: string
    'stroke-dashoffset'?: string
    'stroke-linecap'?: string
    'stroke-linejoin'?: string
    'stroke-miterlimit'?: string
    'stroke-opacity'?: string
    'stroke-width'?: string
    'text-anchor'?: string
    'text-decoration'?: string
    'text-rendering'?: string
    'unicode-bidi'?: string
    'visibility'?: string
    'word-spacing'?: string
    'writing-mode'?: string
  }

  export interface TransferFunctionAttributes {
    type?: string
    tableValues?: string
    slope?: string
    intercept?: string
    amplitude?: string
    exponent?: string
    offset?: string
  }

  export interface XLinkAttributes {
    'xlink:href'?: string
    'xlink:type'?: string
    'xlink:role'?: string
    'xlink:arcrole'?: string
    'xlink:title'?: string
    'xlink:show'?: string
    'xlink:actuate'?: string
  }
}
