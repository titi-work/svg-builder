import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  FilterPrimitiveAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
  PresentationAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface ForeignObjectAttributes extends ConditionalProcessingAttributes, CoreAttributes, FilterPrimitiveAttributes, GraphicalEventAttributes, PositionAttributes, PresentationAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class ForeignObject extends Element {
  constructor(attrs: ForeignObjectAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'foreignObject'

    this.permittedContent = 'any'
    this.initialize()
  }
}

export default ForeignObject
