import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
  PresentationAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface LineAttributes extends ConditionalProcessingAttributes, CoreAttributes, GraphicalEventAttributes, PositionAttributes, PresentationAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class Line extends Element {
  constructor(attrs: LineAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'line'

    this.permittedContent = ['animation', 'descriptive']
    this.initialize()
  }
}

export default Line
