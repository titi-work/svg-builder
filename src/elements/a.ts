import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
  PresentationAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface AnchorAttributes extends ConditionalProcessingAttributes, CoreAttributes, PositionAttributes, GraphicalEventAttributes, PresentationAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class A extends Element {
  constructor(attrs: AnchorAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'a'
    this.permittedContent = 'any'
    this.initialize()
  }
}

export default A
