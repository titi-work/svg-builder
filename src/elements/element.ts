import type SvgBuilder from '../../index'

type Attributes = Record<string, any>

class Element {
  name: string = ''
  attributes: Attributes
  content?: typeof SvgBuilder
  node: string = ''
  permittedContent: any

  constructor(attrs: Attributes, content?: typeof SvgBuilder) {
    this.attributes = attrs
    this.content = content
  }

  protected initialize(): void {
    this.make(this.attributes)
  }

  getElementName(element: string): string {
    return element.match(/(\w+)/)![0]
  }

  protected make(attrs: Attributes): void {
    let element = `<${this.name}`
    let prop: string

    if (attrs) {
      for (prop in attrs) {
        element += ` ${prop}="${attrs[prop]}"`
      }
    }

    element += '>'
    this.node = element
  }
}

export default Element
