import type SvgBuilder from '../../index'
import Element from './element'

export interface StyleAttributes {
  type?: string
  media?: string
  title?: string
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class Style extends Element {
  constructor(attrs: StyleAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'style'

    this.permittedContent = 'any'
    this.initialize()
  }
}

export default Style
