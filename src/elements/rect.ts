import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
  PresentationAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface RectAttributes extends ConditionalProcessingAttributes, CoreAttributes, GraphicalEventAttributes, PositionAttributes, PresentationAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

export interface RectContent {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class Rect extends Element {
  constructor(attrs: RectAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'rect'

    this.permittedContent = ['animation', 'descriptive']
    this.initialize()
  }
}

export default Rect
