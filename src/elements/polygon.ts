import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
  PresentationAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface PolygonAttributes extends ConditionalProcessingAttributes, CoreAttributes, GraphicalEventAttributes, PositionAttributes, PresentationAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class Polygon extends Element {
  constructor(attrs: PolygonAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'polygon'

    this.permittedContent = ['animation', 'descriptive']
    this.initialize()
  }
}

export default Polygon
