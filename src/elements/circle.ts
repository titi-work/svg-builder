import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
  PresentationAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface CAttributes extends GraphicalEventAttributes, CoreAttributes, PositionAttributes, PresentationAttributes, ConditionalProcessingAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class Circle extends Element {
  constructor(attrs: CAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'circle'

    this.permittedContent = ['animation', 'descriptive']
    this.initialize()
  }
}
export default Circle
