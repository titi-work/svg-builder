import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
  PresentationAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface PathAttributes extends ConditionalProcessingAttributes, CoreAttributes, GraphicalEventAttributes, PositionAttributes, PresentationAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class Path extends Element {
  constructor(attrs: PathAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'path'

    this.permittedContent = ['animation', 'descriptive']
    this.initialize()
  }
}

export default Path
