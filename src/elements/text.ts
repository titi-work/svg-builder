import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
  PresentationAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface TextAttributes extends ConditionalProcessingAttributes, CoreAttributes, GraphicalEventAttributes, PositionAttributes, PresentationAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class Text extends Element {
  constructor(attrs: TextAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)
    this.name = 'text'

    this.permittedContent = [
      'animation',
      'descriptive',
      'textcontentchild',
      'container a',
    ]
    this.initialize()
  }
}

export default Text
