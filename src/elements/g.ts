import type {
  ConditionalProcessingAttributes,
  CoreAttributes,
  GraphicalEventAttributes,
  PositionAttributes,
} from '@svg-builder/types/attributes'
import type SvgBuilder from '../../index'
import Element from './element'

export interface GAttributes extends GraphicalEventAttributes, CoreAttributes, ConditionalProcessingAttributes, PositionAttributes {
  style?: string
  class?: string
  externalResourcesRequired?: string
  transform?: string
}

class G extends Element {
  constructor(attrs: GAttributes, content?: typeof SvgBuilder) {
    super(attrs, content)

    this.name = 'g'

    this.permittedContent = 'any'
    this.initialize()
  }
}

export default G
